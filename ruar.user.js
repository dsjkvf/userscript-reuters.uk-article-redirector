// ==UserScript==
// @name        reuters.uk article redirector
// @namespace   oyvey
// @homepage    https://bitbucket.org/dsjkvf/userscript-reuters.uk-article-redirector
// @downloadURL https://bitbucket.org/dsjkvf/userscript-reuters.uk-article-redirector/raw/master/ruar.user.js
// @updateURL   https://bitbucket.org/dsjkvf/userscript-reuters.uk-article-redirector/raw/master/ruar.user.js
// @include     https://uk.reuters.com/article/*
// @run-at      document-start
// @version     1.01
// @grant       none
// ==/UserScript==

var newLocation = window.location.protocol + '//'
            + window.location.host.replace('uk.', 'www.')
            + window.location.pathname;
window.location.replace(newLocation);
