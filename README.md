userscript-reuters.uk-article-redirector
========================================

## About

This is a simple [userscript](https://github.com/OpenUserJs/OpenUserJS.org/wiki/Userscript-beginners-HOWTO) for [Greasemonkey](https://addons.mozilla.org/firefox/addon/greasemonkey/) or similar extension, which -- upon visiting an article published in the [UK Reuters edition](https://uk.reuters.com) -- will instantly redirect to the same article at the main site.
